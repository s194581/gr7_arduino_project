#include "LCDhelper.h"
#include "DHThelper.h"
#include "smartCurtain.h"
#include "spiSlave.h"



//Set prevTime to -10000 to read sensor when turning on.
unsigned long prevTime = -10000;

unsigned long timeDelay = 1000;

uint8_t previousState = 0;



struct DataPoints dataPoints;

void setup()
{ 

  initSlaveSPI();
  LCDInit();
  Serial.begin(115200);
  DHTinit();
  initCurtain();
}
int currentState = 0;
void loop() 
{
  // Serial.print(currentState);
  // Serial.print("\t");
  // Serial.println(previousState);
  // put your main code here, to run repeatedly:
  Alarm.delay(1000);
  registerBtnClick();

  displayToLCD(dataPoints);
  if (it_flag == 1 && flagSetTime == 1)
  {
     syncAlarm(dataPoints.sunrise, dataPoints.sunset, dataPoints.dt); //sync uno clock and sync alarms with api data
  }
  // read DHT sensor;
  if ((millis() - prevTime) > timeDelay) { // 100 ms update
    dataPoints.localTemp = 0.8*dataPoints.localTemp+0.2*getTemperature();
    dataPoints.localHumid = 0.8*dataPoints.localHumid+0.2*getHumidity();

    TxHumid.ulong = dataPoints.localHumid*100;
    TxTemp.ulong = dataPoints.localTemp*100;

    prevTime = millis();
  }
  if (it_flag == 1)
  {
    it_flag = 0; // reset flag
    packTxData(TxBuf); // sets data for next transfer.
    unpackRxData(RxBuf);
    dataPoints.APItemp = ((double)((int32_t)RxAPItemp.ulong))/100;
    dataPoints.sunset = RxAPIsunset.ulong;
    dataPoints.sunrise = RxAPIsunrise.ulong;
    dataPoints.dt = RxAPIdt.ulong;
    // Serial.print("API temperature: ");
    // Serial.println(dataPoints.APItemp,DEC);
    // Serial.print("API sunset: ");
    // Serial.println(dataPoints.sunset,DEC);
    // Serial.print("API sunrise: ");
    // Serial.println(dataPoints.sunrise,DEC);
    // Serial.print("API dt: ");
    // Serial.println(dataPoints.dt,DEC);
    // Serial.println("__________________________");
    if(verify_checksum(RxBuf,Rxchecksum) != 1)
    {
      // Print previous API value.
    }
  }
  // buttons for curtain  
  if (configButton() == 1)
  {
    controlFlag = 1;
    controlStep("config");
  }
  else if (manualButton() == 1)
  {
    controlFlag = 1;
    controlStep("manual");
  }
  // Weather alarm set and remove alarms, and sync sunrise and sunset at specific time 
  if(hourT == 02 && minT == 00 && secT >= 00)
  {
    // dayLate = weekday(t);//This is a test setup
    flagSetTime = 1;
  }
  else if(controlFlag == 1 && HourM >= hourT && MinM >= minT)
  {
    // Serial.println("Trigger ControlFlag both");
    Alarm.free(morningId);
    Alarm.free(eveningId);
    controlFlag = 0;
  }
  else if(controlFlag == 1 &&  HourM <= hourT && MinM <= minT && SecM <= secT && HourE >= hourT && MinE >= minT)
  {  
    // Serial.println("Trigger ControlFlag evening");
    Alarm.free(eveningId);
    controlFlag = 0;
  }
  //Serial.print(analogRead(curtainStatePin));
  // Checks for the state of the pin being HIGH or LOW on curtainStatePin 
  // If a change has occured the curtain will be changed to new state (open/closed)
  if (analogRead(curtainStatePin) > 512)
  {
    currentState = 1;
    if (previousState != currentState)
    {
      previousState = currentState;
      openCloseCurtains("open");
    }
  }
  else
  {
    currentState = 0;
    if (previousState != currentState)
    {
      previousState = currentState;
      openCloseCurtains("close");
    }
  }
}

// SPI interrupt routine
ISR (SPI_STC_vect)
{
  byte c = SPDR;  // Rx
  SPDR = (uint8_t)TxBuf[pos]; // Tx

  RxBuf[pos] = c; // Buckets in to RxBuffer

  pos++;          // increments array position

  if (pos >= SPI_BUFFER_LENGTH) // last array position
  {
    it_flag = 1; // set interrupt flag
    pos = 0;        // reset buffer position
  }
}
