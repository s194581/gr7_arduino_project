#include <LiquidCrystal_I2C.h>  // Library for LCD
#include "DataPointsStruct.h";

const int buttonPin = 2;
LiquidCrystal_I2C lcd(0x27, 16, 2);
int i = 0;
int prevI = 0;

void printToLCD(String text, double value, String unit);

void LCDInit() {
  lcd.init();       //initialize the lcd
  lcd.backlight();  //open the backlight
  pinMode(buttonPin, INPUT_PULLUP);
}

void registerBtnClick() {
  if (digitalRead(buttonPin) == LOW) {
    i = (i + 1) % 3;
    delay(200);
    if (prevI != i) {
      lcd.clear();
    }
    prevI = i;
  }
}

void displayToLCD(struct DataPoints dataPoints) {
  switch (i) {
    case 0:
      printToLCD("Temperature", dataPoints.localTemp, String((char)223) + "C");
      break;

    case 1:
      printToLCD("Humidity", dataPoints.localHumid, String((char)37));
      break;
    
    case 2:
      printToLCD("Copenhagen Temp", dataPoints.APItemp, String((char)223) + "C");
      break;

    default:
      printToLCD("Something went wrong", -1, "");
      break;
  }
}

void printToLCD(String text, double value, String unit) {
  String newVal = String(value) + unit;
  lcd.setCursor(0, 0);
  lcd.print(text + ":");
  lcd.setCursor(0, 1);
  lcd.print(newVal);
}
