#include "HardwareSerial.h"
#include "DHT.h";

#define DHTTYPE DHT11
#define DHTPIN 3
#

DHT dht(DHTPIN, DHTTYPE);


void DHTinit() {
  dht.begin();
}

double getHumidity(){
  double humid = dht.readHumidity();
  return humid;
}

double getTemperature(){
  // Read temperature as Celsius (the default)
  double temp = dht.readTemperature();
  return temp;
}