#include <TimeLib.h>
#include <TimeAlarms.h>
#include <Stepper.h> //https://projecthub.arduino.cc/BEASTIDREES62/driving-28byj-48-stepper-motor-control-with-joystick-628507
#include <EEPROM.h>

#include <LiquidCrystal_I2C.h>

//  define number of steps per revolution
#define STEPS 32
 
// define stepper  motor control pins
#define IN1  4
#define IN2  5
#define IN3  6
#define IN4  7

// define curtain control pins
#define joystick  A0              // Joystick controlling the position of the curtains in configuration mode
#define cButton 9                 // Button for starting and ending the configuration of curtains
#define mButton 8                 // Manual curtain control button
#define curtainStatePin A1         //
//#define pResistor A2              // Photoresistor on analog pin 2, 10-bit ADC (NOT USED)
//#define pSensitivity 650          // Set this value for the sensitivity of the photoresistor (NOT USED)
//#define measures 10000            // amount of measures (NOT USED)

//-------------------------------------------------------- Globals ----------------------------------------------------------

// Stepper, configuration and manual control globals
int configCurtain;                // Holds the stepper position value for closed curtains/blinds (2038 is one full rotation)
long position = 0;                // Position of the stepper motor controlling the curtains

long previousMillis = 0;          // Holds the previous millis() value, used with global variable interval and local variable currentMillis to update sensor variables

unsigned long configMillis = 0;         // Holds the previous millis() value, used with currentConfigMillis to check configButton has been pressed for the chosen configInterval
unsigned long currentConfigMillis = 0;  // Holds the current millis() value, used with configMillis to check configButton has been pressed for the chosen configInterval

unsigned long manualMillis = 0;         // Holds the previous millis() value, used with currentConfigMillis to check configButton has been pressed for the chosen configInterval
unsigned long currentManualMillis = 0;  // Holds the current millis() value, used with configMillis to check configButton has been pressed for the chosen configInterval

uint8_t configFlag = 0;                 // Flag used to indicate configuration has been initiated
uint8_t manualFlag = 0;
uint8_t cButtonState = 0;
uint8_t cLastButtonState = 0;
uint8_t mButtonState = 0;
uint8_t mLastButtonState = 0;
unsigned int configInterval = 5000;
unsigned int manualInterval = 1000;
// int pCounter = 0;
// unsigned long pVals = 0;

// Alarm/ time Globals
// For synchronization
int SyncH = 18;
int SyncM = 49;
int SyncS = 30;
int SyncMonth = 1;
int SyncDay = 1;
int SyncYear = 2024;

// For morning sunrise
int HourM = 18;
int MinM = 50;
int SecM = 0;
//MonthM = 1;   // May be used later
//DayM = 1;     // May be used later
//YearM = 2024; // May be used later

// For evening sundown
int HourE = 18;
int MinE = 51;
int SecE = 0;
//MonthE = 1;   // May be used later
//DayE = 1;     // May be used later
//YearE = 2024; // May be used later


// Controls
int flagSetTime = 0;
int controlFlag = 0;
int dayLate = 0;
int dayNow = 0;
int hourT = 0;
int minT = 0;
int secT = 0;
unsigned long t= 0;

// From weather API
uint32_t sunrise;
uint32_t sunset;
uint32_t timeS;
//------------------------------------------------------ Initializations ------------------------------------------------------

// Initialize alarm tags
AlarmId morningId;
AlarmId eveningId;

// initialize stepper library
Stepper stepper(STEPS, IN4, IN2,  IN3, IN1);

// Initialize LCD screen
//LiquidCrystal_I2C lcd(0x27, 16, 2);





//-------------------------------------------------------- Functions ----------------------------------------------------------

void initCurtain();
void openCloseCurtains(char state[5]);
void curtainControl(int input);
uint8_t configButton();
uint8_t manualButton();
void controlStep(char state[6]);
void MorningAlarm();
void EveningAlarm();
void syncAlarm(uint32_t sunrise, uint32_t sunset, uint32_t timeS);
void digitalClockDisplay();
void printDigits(int digits);
void Repeats2();

void initCurtain()
{  
  // for testing alarm
  // setTime(SyncH,SyncM,SyncS,SyncDay,SyncMonth,SyncYear);
  // Alarm.timerRepeat(5, Repeats2);

  // use only to clear EEPROM
  // EEPROM.write(0, 0);
  // EEPROM.write(1, 0);
  // EEPROM.write(2, 0);
  // EEPROM.write(3, 0);
  // EEPROM.write(4, 0);
  // EEPROM.write(5, 0);
  // EEPROM.write(6, 0);
  // EEPROM.write(7, 0);
  
  
  pinMode(configButton, INPUT);
  pinMode(manualButton, INPUT);
  pinMode(curtainStatePin, INPUT);
  EEPROM.get(0, configCurtain);
  //Serial.println(configCurtain);
  EEPROM.get(4, position);
  //Serial.println(position);

  flagSetTime = 1; 

  //dayLate = weekday(t);   // May be used later
  // create the alarms 
  morningId=Alarm.alarmOnce(HourM,MinM,SecM, MorningAlarm);  // morningAlarm set on startup
  eveningId=Alarm.alarmOnce(HourE,MinE,SecE,EveningAlarm);  // eveningAlarm set on startup
}


// This function opens and closes the curtain
void openCloseCurtains(char state[5])
{
  if (state == "open"){
    while(position != 0)
	  {
      stepper.setSpeed(500);
      stepper.step(-1);
      position--;
    }
  }
  else if (state == "close"){
    while(position != configCurtain)
    {
      stepper.setSpeed(500);
      stepper.step(1);  
      position++;
    }
  }
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, LOW);
  digitalWrite(IN3, LOW);
  digitalWrite(IN4, LOW);
  EEPROM.write(4, position);
}

// Opens or closes the curtain depending on the photoresistors input value 
// FUNCTION NOT USED
// void curtainControl(int input)
// {
//   if (input > pSensitivity)
//  {
//     openCloseCurtains("close");
//     EEPROM.put(4, position);
//   }
//   else{
//     openCloseCurtains("open");
//     EEPROM.put(4, position);
//   }
// } 

// Function "configButton" checks if the button has been pressed for atleast a few seconds
uint8_t configButton()
{
  cButtonState = digitalRead(cButton);

  if (cButtonState != cLastButtonState)
  {
    cLastButtonState = cButtonState;
    if (cButtonState == 1)
    {
      configMillis = millis();
      configFlag = 1;
    }
    else if (cButtonState == 0)
    {
      configMillis = 0;
      configFlag = 0;
      currentConfigMillis = 0;
    }
  }

  if (configFlag == 1)
  {
    currentConfigMillis = millis();
  }
  if (currentConfigMillis - configMillis > configInterval)
  {
    configFlag = 0;
    currentConfigMillis = 0;
    configMillis = 0;
    cButtonState = 0;
    cLastButtonState = 0;
    return 1;
  }
  return 0;
}


// "Function manualButton" Checks if the button has been pressed for atleast a few seconds and returns 1 once condition is fulfilled
uint8_t manualButton()
{
  mButtonState = digitalRead(mButton);

  if (mButtonState != mLastButtonState)
  {
    mLastButtonState = mButtonState;
    if (mButtonState == 1)
    {
      manualMillis = millis();
      manualFlag = 1;
    }
    else if (mButtonState == 0)
    {
      manualMillis = 0;
      manualFlag = 0;
      currentManualMillis = 0;
    }
  }

  if (manualFlag == 1)
  {
    currentManualMillis = millis();
  }
  if (currentManualMillis - manualMillis > manualInterval)
  {
    manualFlag = 0;
    currentManualMillis = 0;
    manualMillis = 0;
    mButtonState = 0;
    mLastButtonState = 0;
    return 1;
  }
  return 0;
}



// Configures the step motor controlling the curtain with the joystick
// When finished by the user: saves the stepper motor position to configCurtain
// and writes the position to the assigned EEPROM addresses.
void controlStep(char state[6])
{
  if (state == "config")
  { 
    uint8_t config = 1;
    int xVal = analogRead(joystick);
    int cButtonState = digitalRead(configButton);

    Serial.println("               Curtain Configuration Mode               ");
    Serial.println("--------------------------------------------------------");
    Serial.println("Use joystick to set closed state ");
    Serial.print("Once finished press and hold black button for ");
    Serial.print(configInterval/1000);
    Serial.println(" second(s)\n\n\n");

    lcd.clear();
    lcd.setCursor(4,0);
    lcd.print("Curtain");
    lcd.setCursor(1,1);
    lcd.print("Config Control");


    while(config != 0)
    {
      xVal  = analogRead(joystick);
      
      if(  (xVal > 500)  && (xVal < 540) )
      {
        digitalWrite(IN1, LOW);
        digitalWrite(IN2, LOW);
        digitalWrite(IN3, LOW);
        digitalWrite(IN4, LOW);
      
      if (configButton() == 1)
        {
          lcd.clear();
          lcd.setCursor(2,0);
          lcd.print("Ended Config"); 
          lcd.setCursor(1,1);
          lcd.print("Position saved");
          
          configCurtain = position;
          Serial.print("Configuration finished with position: ");
          Serial.println(configCurtain);
          Serial.println("Saved to EEPROM");
          EEPROM.put(0, configCurtain);
          EEPROM.put(4, position);
          delay(3000);
          lcd.clear();
          config = 0;
        }
      }
      else
      {
        while (xVal  >= 540)
        {
          int speed_  = map(xVal, 540, 1023, 5, 500);
          stepper.setSpeed(speed_);
          stepper.step(-1);
          position--;
          xVal = analogRead(joystick);
        }
        while (xVal <= 500)
        {
          int speed_ = map(xVal, 500, 0, 5, 500);
          stepper.setSpeed(speed_);
          stepper.step(1);
          position++;
          xVal = analogRead(joystick);
        }
      }
    }
  }
  else if (state == "manual")
  {
    uint8_t manual = 1;
    int xVal = analogRead(joystick);
    int mButtonState = digitalRead(manualButton);


    lcd.clear();
    lcd.setCursor(4,0);
    lcd.print("Curtains");
    lcd.setCursor(1,1);
    lcd.print("Manual Control");

    Serial.println("              Curtain Manual Mode               ");
    Serial.println("-------------------------------------------------------");
    Serial.println("Use joystick to set curtain position");
    Serial.print("Once finished press and hold green button for ");
    Serial.print(manualInterval/1000);
    Serial.println(" second(s)\n\n\n");
    while(manual != 0)
    {
      xVal  = analogRead(joystick);
      
      if (manualButton() == 1)
        {
          Serial.println("Finished manual curtain control");
          EEPROM.put(4, position);
          lcd.clear();
          manual = 0;
        }

      if((xVal > 500) && (xVal < 540))
      {
        digitalWrite(IN1, LOW);
        digitalWrite(IN2, LOW);
        digitalWrite(IN3, LOW);
        digitalWrite(IN4, LOW);
        
      }
      else
      {
        while (xVal  >= 540)
        {
          int speed_  = map(xVal, 540, 1023, 5, 500);
          stepper.setSpeed(speed_);
          stepper.step(-1);
          position--;
          xVal = analogRead(joystick);
        }
        while (xVal <= 500)
        {
          int speed_ = map(xVal, 500, 0, 5, 500);
          stepper.setSpeed(speed_);
          stepper.step(1);
          position++;
          xVal = analogRead(joystick);
        }
      }
    }
  }
}

// functions to be called when an alarm triggers:
void MorningAlarm()
{ 
  openCloseCurtains("close");
}

void EveningAlarm()
{
  openCloseCurtains("open");          
}

void syncAlarm(uint32_t sunrise, uint32_t sunset, uint32_t timeS)
{
  //get sunrise and sundown;

      // convert from unix time
        HourM = hour(sunrise);
        MinM = minute(sunrise);
        SecM = second(sunrise);

        HourE = hour(sunset);
        MinE = minute(sunset);
        SecE = second(sunset);

        SyncH=hour(t);
        SyncM=minute(t);
        SyncS=second(t);
        
        //Set clock on uno and pull down flag
        setTime(SyncH,SyncM,SyncS,SyncDay,SyncMonth,SyncYear);
 
        morningId=Alarm.alarmOnce(HourM,MinM,SecM, MorningAlarm);  // morningAlarm set on startup
        eveningId=Alarm.alarmOnce(HourE,MinE,SecE, EveningAlarm);  // eveningAlarm set on startu
        flagSetTime = 0;    
}

void Repeats2() {
  digitalClockDisplay();
}

void digitalClockDisplay() {
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.println();
}


void printDigits(int digits) {
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

