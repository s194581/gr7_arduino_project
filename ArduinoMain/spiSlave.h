
#include <SPI.h>
#include <stdint.h>
#include <string.h>
// #include "DataPointsStruct.h"

#define SS_PIN 10

typedef enum SPI_TX{
  TxHumid0 = 0,
  TxHumid1,
  TxHumid2,
  TxHumid3,       // 32 bit
  TxTemp0,
  TxTemp1,
  TxTemp2,
  TxTemp3,        // 32 bit
  TxPhotores0,
  TxPhotores1,    // 16 bit
}SPI_TX;

typedef enum{
  RxAPItemp0 = 0,
  RxAPItemp1,
  RxAPItemp2,
  RxAPItemp3,
  RxAPIsunset0,
  RxAPIsunset1,
  RxAPIsunset2,
  RxAPIsunset3,
  RxAPIsunrise0,
  RxAPIsunrise1,
  RxAPIsunrise2,
  RxAPIsunrise3,
  RxAPIdt0,
  RxAPIdt1,
  RxAPIdt2,
  RxAPIdt3,
  ChecksumLsb,
  ChecksumMsb,  // 16 bit   
  SPI_BUFFER_LENGTH
}SPI_RX;


char RxBuf[SPI_BUFFER_LENGTH];
uint8_t TxBuf[SPI_BUFFER_LENGTH] = "Hello Master";
uint8_t Txchecksum1, Txchecksum2;
uint8_t Rxchecksum1, Rxchecksum2;
uint16_t Rxchecksum = 0;


typedef union TxDHT{
  uint8_t b[4];
  unsigned long ulong;
}TxDHT;

TxDHT TxHumid;
TxDHT TxTemp;
TxDHT RxAPItemp;
TxDHT RxAPIsunset;
TxDHT RxAPIsunrise;
TxDHT RxAPIdt;


uint8_t pos;
uint8_t it_flag;


void initSlaveSPI()
{
  pinMode(MISO, OUTPUT); // Slave so MISO is output

  // turn on SPI in slave mode
  SPCR |= _BV(SPE);
  
  pos = 0;   // buffer empty
  it_flag = 0;

  // now turn on interrupts
  SPI.attachInterrupt();

  SPI.setBitOrder(LSBFIRST);   // Most systems use MSB first

  uint8_t empty = 0;
  for (int i = 0; i<4; i++) // read out anything from Rx fifo in to clear it
  {
    empty = SPDR;
  }
}

uint8_t verify_checksum(uint8_t* data, uint16_t checksum)
{
  if (checksum == data[ChecksumLsb]+((uint16_t)data[ChecksumMsb] << 8))
  {
    //Serial.println("Checksum Verified");
    return 1;
  }
  else
  {
    //Serial.println("Checksum Invalid");
    return 0;
  }
}


void calculate_fletcher_checksum(uint8_t* data, int length, uint8_t* checksum1, uint8_t* checksum2) 
{
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    int i;

    for (i = 0; i < length; ++i) {
        sum1 = (sum1 + (uint16_t)data[i]) % 255;
        sum2 = (sum2 + sum1) % 255;
    }

    // Serial.print("sum1 checksum: ");
    // Serial.println(sum1,HEX);
    // Serial.print("sum2 checksum: ");
    // Serial.println(sum2,HEX);

    *checksum1 = (uint8_t)sum1;
    *checksum2 = (uint8_t)sum2;
}

void packTxData(char * data)
{
  // TxHumid = *(uint64_t*)&dataPoints.localHumid;
  memcpy(&data[TxHumid0], &TxHumid.b, sizeof(unsigned long)); // copying data to txbuffer
  memcpy(&data[TxTemp0], &TxTemp.b, sizeof(unsigned long)); // copying data to txbuffer
  data[TxPhotores0] = 255; // LSB
  data[TxPhotores1] = 255; // MSB
  
  calculate_fletcher_checksum((uint8_t*)data,SPI_BUFFER_LENGTH-2,&Txchecksum1,&Txchecksum2);
  data[ChecksumLsb] = Txchecksum1; // needs to be implemented.
  data[ChecksumMsb] = Txchecksum2; // needs to be implemented.

}

void unpackRxData(char * data)
{
  memcpy(&RxAPItemp.b,&data[RxAPItemp0],sizeof(uint32_t));
  memcpy(&RxAPIsunset.b,&data[RxAPIsunset0],sizeof(uint32_t));
  memcpy(&RxAPIsunrise.b,&data[RxAPIsunrise0],sizeof(uint32_t));
  memcpy(&RxAPIdt.b,&data[RxAPIdt0],sizeof(uint32_t));



  calculate_fletcher_checksum(data,SPI_BUFFER_LENGTH-2,&Rxchecksum1,&Rxchecksum2);
  Rxchecksum = Rxchecksum1 + ((uint16_t)Rxchecksum2 << 8);
  // Serial.print("Rx Checksum measured: ");
  // Serial.print(data[ChecksumMsb],HEX);
  // Serial.println(data[ChecksumLsb],HEX);
  // Serial.print("Rx checksum calculated: ");
  // Serial.println(Rxchecksum,HEX);

}







