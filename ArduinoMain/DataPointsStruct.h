//struct of different data that can be displayed {localTemp, localHumid}
struct DataPoints{
  double localTemp;
  double localHumid;
  double APItemp;
  uint32_t sunset;
  uint32_t sunrise;
  uint32_t dt;
};