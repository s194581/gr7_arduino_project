#include <ESP8266WiFiMulti.h>   // Include the Wi-Fi-Multi library
#include <ESP8266WebServer.h>   // Include the WebServer library
#include <ESP8266mDNS.h>        // Include the mDNS library


#define curtainStatePin 5
#define netName "Bille"
#define netPass "12345678"
// Initialyze wifiMulti
ESP8266WiFiMulti wifiMulti;
// Create an instance of the server
ESP8266WebServer server(80);
struct DataPoints* dataPointsH;

void initWebserver();
void handleRoot();  
void handleCurtain();  
void handleNotFound();

void initWebserver(struct DataPoints* dataPoints) 
{
  dataPointsH = dataPoints;
  pinMode(curtainStatePin, OUTPUT);
  //digitalWrite(curtainStatePin,1);
  
  //wifiMulti.addAP("IoTFotonik", "Cyberteknologi");  // add Wi-Fi networks you want to connect to
  wifiMulti.addAP(netName, netPass);

  if (MDNS.begin("iot"))  // Start the mDNS responder for esp8266.local
  {             
    Serial.println("mDNS responder started");
  } else {
    Serial.println("Error setting up MDNS responder!");
  }

  server.on("/", HTTP_GET, handleRoot);
  server.on("/Curtain", HTTP_POST, handleCurtain);
  server.onNotFound(handleNotFound);
    
  // Start the server
  server.begin();
  Serial.println("Server started"); 
}

void handleRoot()   // When URI / is requested, send a web page with a button to toggle the LED
{
  server.send(200, "text/html", "<html><title>Internet of Things - Group 7 Demonstration</title><meta charset=\"utf-8\" \/> \
      </head><body><h1>Welcome to Group 7 Home Automation Webserver for curtain control</h1> \ curtainStatePin \
      <p>To open/ close curtains press button below and give it some time to change curtains to the new state</p> \
      <form action=\"/Curtain\" method=\"POST\" ><input type=\"submit\" value=\"Change State of Curtains\" style=\"width:500px; height:100px; font-size:24px\"></form> \
      </body></html>");
}

void handleCurtain() // If a POST request is made to URI /LED
{                          
  if (digitalRead(curtainStatePin) == LOW)
  {
    dataPointsH -> curtainState = 1;
    digitalWrite(curtainStatePin, HIGH);  
  }
  else
  {
    dataPointsH -> curtainState = 0;
    digitalWrite(curtainStatePin, LOW);
  }

  //digitalWrite(curtainStatePin,!digitalRead(curtainStatePin));      // Change the state of the LED
  
  server.sendHeader("Location","/");        // Add a header to respond with a new location for the browser to go to the home page again
  server.send(303);                 // Send it back to the browser with an HTTP status 303 (See Other) to redirect
  
  // if(dataPointsH -> curtainState == 0) {
  //   dataPointsH -> curtainState = 1;
  //   digitalWrite(curtainStatePin, HIGH);
  // } 
  // else 
  // {
  //   dataPointsH -> curtainState = 0;
  //   digitalWrite(curtainStatePin, LOW);
  // }
  Serial.print("State: ");
  Serial.println(digitalRead(curtainStatePin)); 
}

void handleNotFound()
{
  server.send(404, "text/plain", "404: Not found"); // Send HTTP status 404 (Not Found) when there's no handler for the URI in the request
}


