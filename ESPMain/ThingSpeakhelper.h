#include <ThingSpeak.h>

unsigned long channelID = 2399735;  //your TS channal
const char* APIKey = "DLANV0S9OL3K5EJW";   //your TS API
const char* tsserver = "api.thingspeak.com";
const int postDelay = 10 * 1000;  //post data every 20 seconds

void writeToThingSpeak(struct DataPoints dataPoints) {
  WiFiClient client;

  ThingSpeak.begin(client);
  client.connect(tsserver, 80);                 //connect(URL, Port)
  ThingSpeak.setField(1, (float) dataPoints.APItemp);
  ThingSpeak.setField(2, (float) dataPoints.localTemp);
  ThingSpeak.setField(3, (float) dataPoints.localHumid);
  ThingSpeak.setField(4, (int) dataPoints.curtainState);                  
  ThingSpeak.writeFields(channelID, APIKey);  //post everything to TS
  client.stop();
  //delay(postDelay);
}