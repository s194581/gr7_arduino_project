#include "ConnectToWeatherApi.h"
#include "ThingSpeakhelper.h"
#include "spiMaster.h"
#include "webserver.h"



//Wifi information
const char* ssid = "Bille";
const char* password = "12345678";

unsigned long prevTimeAPIcall = -15000;
unsigned long prevTimeThingSpeak = -15000;
// Timer set to 10 minutes (600000)
unsigned long timeDelayAPIcall = 15000;
unsigned long timeDelayThingSpeak = 15000;
uint32_t prevSpiUpdate = 0;
uint8_t curtain = 0;


struct DataPoints dataPoints;
struct DataPoints* dataPointsPtr = &dataPoints;

void setup() {

  Serial.begin(115200);
  //initialize spi
  initSpiMaster();
  //initialises wifi connection
  initWifi();
  initWebserver(dataPointsPtr);
}

void loop() {
  // Check if a client has connected
  server.handleClient();

  // Send an HTTP GET request
  if ((millis() - prevTimeAPIcall) > timeDelayAPIcall) {
    //Gets weather Data (Atm no data return just serial prints).
    getWeatherData(dataPointsPtr);
    Serial.print("Temperature API: ");
    Serial.println((dataPoints.APItemp));
    prevTimeAPIcall = millis();
  }
  if ((millis() - prevTimeThingSpeak) > timeDelayThingSpeak) {
    if (verify_checksum(RxBuf, Rxchecksum) != 0) {
    }
    Serial.println("Writing to thingspeak...");
    writeToThingSpeak(dataPoints);
    prevTimeThingSpeak = millis();
  }
  if ((millis() - prevSpiUpdate) > 15000)//spiUpdatePeriod)  // should be 15-20 seconds
  {
    Serial.print("API temp typecasted scaled by x100: ");
    Serial.println((int32_t)(dataPoints.APItemp * 100));
    setTxBuf(TxBuf, (int32_t)(dataPoints.APItemp * 100), dataPoints.sunset, dataPoints.sunrise, dataPoints.dt);
    unpackRxData(RxBuf);
    transmitSPI(&TxBuf[0], SPI_BUFFER_LENGTH);
    dataPoints.localHumid = ((double)((int32_t)RxHumid.ulong)) / 100;
    dataPoints.localTemp = ((double)((int32_t)RxTemp.ulong)) / 100;
    Serial.println("Local Humid: ");
    Serial.println(dataPoints.localHumid, DEC);
    Serial.println("Local Temp:");
    Serial.println(dataPoints.localTemp, DEC);
    Serial.println("Photores: ");
    Serial.println(photoRes, DEC);
    Serial.println("Rx Checksum Measured: ");
    Serial.print(RxBuf[ChecksumMsb], HEX);
    Serial.println(RxBuf[ChecksumLsb], HEX);
    Serial.println("Rx Checksum Calculated: ");
    Serial.println(Rxchecksum, HEX);
    // for (int i = 0; i < SPI_BUFFER_LENGTH; i++) {
    //   Serial.print(RxBuf[i],HEX);
    // }
    Serial.println("___________________________");
    prevSpiUpdate = millis();
  }

  // // Control curtain on the Arduino UNO
  // if (dataPointsPtr -> curtainState == 1)
  // {
  //   digitalWrite(5, HIGH);
  // }
  // else
  // {
  //   digitalWrite(5, LOW);
  // }
}


//Initializes Wifi and connects to the declared wifi
void initWifi() {
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while (WiFi.status() != WL_CONNECTED) {
    static uint8_t timeout = 0;
    delay(500);
    Serial.print(".");
    timeout++;
    if (timeout > 20) {
      break;
    }
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}
