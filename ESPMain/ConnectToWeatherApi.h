#include "HardwareSerial.h"
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <Arduino_JSON.h>
#include "DataPointsStruct.h"

//APi key
String openWeatherMapApiKey = "49d66b2351a58e635134cef7bc601091";


// country code and city for weather
String city = "Copenhagen";
String countryCode = "DK";


String jsonBuffer;

String httpGETRequest(const char* serverName) {
  WiFiClient client;
  HTTPClient http;

  //Domain name
  http.begin(client, serverName);

  // Send HTTP POST request
  int httpResponseCode = http.GET();

  String payload = "{}";

  if (httpResponseCode > 0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  } else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  // Free resources
  http.end();

  return payload;
}

void getWeatherData(struct DataPoints* dataPoints) {

  // Send an HTTP GET request
  // Check WiFi connection status
  if (WiFi.status() == WL_CONNECTED) {
    String serverPath = "http://api.openweathermap.org/data/2.5/weather?q=" + city + "," + countryCode + "&APPID=" + openWeatherMapApiKey;

    jsonBuffer = httpGETRequest(serverPath.c_str());

    JSONVar myObject = JSON.parse(jsonBuffer);

    // JSON.typeof(jsonVar) can be used to get the type of the var
    if (JSON.typeof(myObject) == "undefined") {
      Serial.println("Parsing input failed!");
      return;
    }

    Serial.println(myObject);
    double temperature = double(myObject["main"]["temp"]) - 273.15;
    uint32_t sunrise = (myObject["sys"]["sunrise"]);
    uint32_t sunset = (myObject["sys"]["sunset"]);
    uint32_t dt = (myObject["dt"]);
    // double windSpeed = double(myObject["wind"]["speed"]);

    dataPoints->APItemp = temperature;
    dataPoints->sunrise = sunrise;
    dataPoints->sunset = sunset;
    dataPoints->dt = dt;

  } else {
    Serial.println("WiFi Disconnected");
  }
}
