/*
    SPI Slave Demo Sketch
    Connect the SPI Master device to the following pins on the esp8266:

    GPIO    NodeMCU   Name  |   Uno
  ===================================
     15       D8       SS   |   D10
     13       D7      MOSI  |   D11
     12       D6      MISO  |   D12
     14       D5      SCK   |   D13

    Note: If the ESP is booting at a moment when the SPI Master has the Select line HIGH (deselected)
    the ESP8266 WILL FAIL to boot!
    See SPISlave_SafeMaster example for possible workaround

*/

#include <SPI.h>
#include <stdint.h>
#include <string.h>

typedef enum{
  RxHumid0 = 0,
  RxHumid1,
  RxHumid2,
  RxHumid3,   // 64 bit
  RxTemp0,
  RxTemp1,
  RxTemp2,
  RxTemp3,       // 64 bit
  RxPhotores0,
  RxPhotores1,    // 16 bit
}SPI_RX;

typedef enum{
  TxAPItemp0 = 0,
  TxAPItemp1,
  TxAPItemp2,
  TxAPItemp3,
  TxAPIsunset0,
  TxAPIsunset1,
  TxAPIsunset2,
  TxAPIsunset3,
  TxAPIsunrise0,
  TxAPIsunrise1,
  TxAPIsunrise2,
  TxAPIsunrise3,
  TxAPIdt0,
  TxAPIdt1,
  TxAPIdt2,
  TxAPIdt3,
  ChecksumLsb,
  ChecksumMsb,  // 16 bit   
  SPI_BUFFER_LENGTH
}SPI_TX;

typedef union RxDHT{
  uint8_t b[4];
  unsigned long ulong;
}RxDHT;

RxDHT RxHumid;
RxDHT RxTemp;

const int SS_PIN = 15;  // GPIO15 for Slave Select
uint8_t Txchecksum1, Txchecksum2, Rxchecksum1, Rxchecksum2;
uint16_t Txchecksum, Rxchecksum;

char receivedbyte = 0;
uint8_t RxBuf[SPI_BUFFER_LENGTH] = { 0 };
uint8_t TxBuf[SPI_BUFFER_LENGTH] = "Hi Slave!";

unsigned long time_now = 0;
uint16_t spiUpdatePeriod = 2000;
unsigned long localTempInt = 0;
unsigned long localHumidInt = 0;
double localTemp = 0;
double localHumid = 0;
uint8_t localHumidBuf[sizeof(double)] = {0};
uint16_t photoRes = 0;


void calculate_fletcher_checksum(uint8_t* data, int length, uint8_t* checksum1, uint8_t* checksum2) 
{
    uint16_t sum1 = 0;
    uint16_t sum2 = 0;
    int i;

    for (i = 0; i < length; ++i) {
        sum1 = (sum1 + (uint16_t)data[i]) % 255;
        sum2 = (sum2 + sum1) % 255;
    }
    Serial.print("sum1 checksum: ");
    Serial.println(sum1,HEX);
    Serial.print("sum2 checksum: ");
    Serial.println(sum2,HEX);

    *checksum1 = (uint8_t)sum1;
    *checksum2 = (uint8_t)sum2;
}

void setTxBuf(uint8_t * data, int32_t APItemp, uint32_t sunset, uint32_t sunrise, uint32_t dt)
{
  memcpy(&data[TxAPItemp0],&APItemp,sizeof(uint32_t));
  memcpy(&data[TxAPIsunset0],&sunset,sizeof(uint32_t));
  memcpy(&data[TxAPIsunrise0],&sunrise,sizeof(uint32_t));
  memcpy(&data[TxAPIdt0],&dt,sizeof(uint32_t));
  // for (int i = 0; i < 4; i++)
  // {
  //   Serial.print(data[TxAPItemp0+i]);
  // }

  calculate_fletcher_checksum(data,SPI_BUFFER_LENGTH-2,&Txchecksum1,&Txchecksum2);
  data[ChecksumLsb] = Txchecksum1;
  data[ChecksumMsb] = Txchecksum2;
  // Serial.println("TxChecksum: ");
  // Serial.print(data[ChecksumMsb],HEX);
  // Serial.print(data[ChecksumLsb],HEX);
  // Serial.println();
}

uint8_t verify_checksum(uint8_t* data, uint16_t checksum)
{
  // Serial.println("Checksum= ");
  // Serial.println((((uint16_t)data[ChecksumMsb])<<8)+data[ChecksumLsb],HEX);
  // Serial.println(checksum,HEX);
  if (checksum == (((uint16_t)data[ChecksumMsb])<<8)+data[ChecksumLsb])
  {
    // Serial.println("Checksum Verified");
    return 1;
  }
  else
  {
    // Serial.println("Checksum Invalid");
    return 0;
  }
}

void transmitSPI(uint8_t * data, uint16_t length) 
{
  
  static uint8_t offset = 1;
  if(verify_checksum(RxBuf,Rxchecksum) != 1)
  {
    // Serial.println("Changing offset");
    // offset++;
    // if (offset == 12)
    // {
    //   offset = 0;
    // }
  }
  for (int i = 0; i < length; i++) 
  {
    static int8_t k = 0;
    k = i-offset;
    if (k < 0) // misalignment in communication is "fixed" here.
    {
      k = i+length-offset;
    }
    digitalWrite(SS_PIN, LOW);          // Select the slave
    RxBuf[k] = SPI.transfer(data[i]);  // Send a byte
    // Serial.print(RxBuf[k],HEX);
    // Serial.print(" - ");
    digitalWrite(SS_PIN, HIGH);  // Deselect the slave
  }
    // Serial.println();
}

void unpackRxData(uint8_t * rxBuf)
{
  // Serial.println("Unpacking...");
  calculate_fletcher_checksum(rxBuf,SPI_BUFFER_LENGTH-2,&Rxchecksum1,&Rxchecksum2);
  Rxchecksum = (uint16_t)Rxchecksum1 + ((uint16_t)Rxchecksum2 << 8);
  for (int i = 0; i < 4; i++)
  {
    RxHumid.b[i] = rxBuf[RxHumid0+i];
    RxTemp.b[i] = rxBuf[RxTemp0+i];
  }
  photoRes=0;
  photoRes = (rxBuf[RxPhotores0]) + ((uint16_t)rxBuf[RxPhotores1]<<8);
}

void initSpiMaster()
{
    // Initialize SPI
  pinMode(SS_PIN, OUTPUT);  // SS must be output for Master mode to work
  SPI.begin();              // Initialize SPI interface

  // Configure SPI
  SPI.setFrequency(50000);     // 50kHz as slow as possible outside dog audible range :')
  SPI.setDataMode(SPI_MODE0);  // Set SPI mode to MODE0
  SPI.setBitOrder(LSBFIRST);   //
}

